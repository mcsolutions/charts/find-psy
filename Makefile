
CHART:=sfpg
URL:=https://mcsolutions.gitlab.io/charts/find-psy/

pack-all:
	@helm package charts/node charts/sfpg
	@mv -f *.tgz public

index:
	@helm repo index . --url $(URL)
	@sed -i 's|public/||g' index.yaml
	@mv -f index.yaml public

template:
	@helm template --debug name-v1 charts/$(CHART) > temp.yaml